		-- SQL MARIADB init sites script --


CREATE DATABASE IF NOT EXISTS wordpress1_db;
-- CREATE USER IF NOT EXISTS 'wordpress1'@'wordpress1.net-1' IDENTIFIED BY 'wordpress';
CREATE USER IF NOT EXISTS 'wordpress1'@'%' IDENTIFIED BY 'wordpresspw';
-- ALTER USER ''wordpress1'@'wordpress1.net-1' IDENTIFIED BY 'wordpresspw';
-- SELECT user, host FROM mysql.user;	
GRANT ALL PRIVILEGES ON wordpress1_db.* TO 'wordpress1'@'%';
FLUSH PRIVILEGES;



CREATE DATABASE IF NOT EXISTS wordpress2_db;
-- CREATE USER IF NOT EXISTS 'wordpress1'@'wordpress1.net-1' IDENTIFIED BY 'wordpress';
CREATE USER IF NOT EXISTS 'wordpress2'@'%' IDENTIFIED BY 'wordpresspw'; 
-- ALTER USER ''wordpress2'@'wordpress2.net-1' IDENTIFIED BY 'wordpresspw';
-- SELECT user, host FROM mysql.user;	
GRANT ALL PRIVILEGES ON wordpress2_db.* TO 'wordpress2'@'%';
FLUSH PRIVILEGES;



CREATE DATABASE IF NOT EXISTS kelsall39_db;
-- CREATE USER IF NOT EXISTS 'wordpress1'@'wordpress1.net-1' IDENTIFIED BY 'wordpress';
CREATE USER IF NOT EXISTS 'wp_trimaran'@'%' IDENTIFIED BY 'wordpresspw'; 
-- ALTER USER ''wordpress2'@'wordpress2.net-1' IDENTIFIED BY 'wordpresspw';
-- SELECT user, host FROM mysql.user;	
GRANT ALL PRIVILEGES ON kelsall39_db.* TO 'wp_trimaran'@'%';
FLUSH PRIVILEGES;